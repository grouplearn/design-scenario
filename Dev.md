# For further Development

## Further requirements
1. Parse more real world data
2. Generate more complex test data


## Structure

- To parse real world data
    - How to do it
        - Transfer the location and host data into the location.json/host.json
        - And add a param.json and config.json manunally
    - The core parameters
        - Find the right neighbours, which has been fullfiled in location.py
        - The location ID should start from 0 in the location.json
        - Find the corresponding location for the host
- Generate further complex test data
    - The scenario may not a square grid, should consider more possibility
    - Scenario Generator can be seen as the base to explore more complex test scenario
